package cop4656.project2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.text.SimpleDateFormat;
import java.util.Date;

import static android.graphics.Typeface.BOLD;

public class ChatActivity extends AppCompatActivity {

    Firebase ref;
    Firebase chatRef;
    ListView lv;
    Button sendButton;
    private FirebaseListAdapter<Message> mAdapter;
    static EditText mMessage;
    String loginName;
    String networkName;
    String BSSID;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Bundle extras = getIntent().getExtras();
        loginName = extras.getString("login");
        networkName = extras.getString("networkName");
        BSSID = extras.getString("BSSID");


        setTitle("Chatting on: " + networkName);

        lv = (ListView) findViewById(R.id.chat_list);

        //create reference to database
        Firebase.setAndroidContext(this);
        ref = new Firebase("https://flickering-fire-6410.firebaseio.com/");
        //set reference to wifi network
        chatRef = ref.child(networkName);

        //listview adapter to populate list with
        //Message object instances from the database.
        //Message defined in Message.class
        mAdapter = new FirebaseListAdapter<Message>(chatRef, Message.class, android.R.layout.two_line_list_item, this) {
            @Override
            protected void populateView(View view, Message message) {
                ((TextView) view.findViewById(android.R.id.text1)).setText(message.getName() + ": " + message.getTime());
                ((TextView) view.findViewById(android.R.id.text2)).setText(message.getContent());
            }
        };

        lv.setAdapter(mAdapter);
        lv.setStackFromBottom(true);
        lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        mMessage = (EditText) findViewById(R.id.edittext_Msg);
        sendButton = (Button) findViewById(R.id.send_button);
        sendButton.setBackgroundColor(0xff0000ff);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void myEventHandler(View v) {
        //if send button clicked, pass message info into Message constructor
        //and push to database

        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy hh:mm aa");
        String time1 = sdf.format(new Date());

        if (v == sendButton) {
            //setValue: if chatRef does not exist, a new chat is created for it
            chatRef.push().setValue(new Message(loginName, mMessage.getText().toString(), time1));
            mMessage.setText("");
        }

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Chat Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    /*
    public class WifiChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            final ConnectivityManager connMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            final NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            if (!wifi.isAvailable()) {
                // Do something

                Log.d("receiver", "Wifi not available");
            }
        }
    }
    */

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAdapter.cleanup();
    }
}
