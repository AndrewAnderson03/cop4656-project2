package cop4656.project2;

import android.content.Intent;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;

import static android.R.attr.data;


public class MainActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final EditText mEdit   = (EditText)findViewById(R.id.editText);
        Button button1 = (Button)findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                final String login = mEdit.getText().toString().trim();
                if (login.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please enter a username",
                            Toast.LENGTH_LONG).show();
                }
                else {
                    WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
                    WifiInfo wifiInfo = wifiManager.getConnectionInfo();

                    if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
                        Intent myIntent = new Intent(MainActivity.this,
                                ChatActivity.class);

                        String networkName = wifiInfo.getSSID();
                        String BSSID = wifiInfo.getBSSID();

                        myIntent.putExtra("networkName", networkName);
                        myIntent.putExtra("BSSID", BSSID);
                        myIntent.putExtra("login", login);
                        startActivity(myIntent);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Please connect to WIFI",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


    }

}
