package cop4656.project2;

/**
 * Created by Andrew on 11/28/2016.
 */
public class Message {
        private String name;
        private String content;
        private String time;

        public Message() {
        }

        public Message(String name, String content, String time) {
            this.name = name;
            this.content = content;
            this.time = time;
        }

        public String getName() {
            return name;
        }

    /*
        public String getUid() {
            return uid;
        }
        */

        public String getContent() {
            return content;
        }

        public String getTime() { return time; }
}

